import sqlite3
import sys
import time
import urllib
import urllib2
import xml.dom.minidom as domlib

def main(args):
	db = sqlite3.connect("var/database.sqlite3")

	if args[0] == "add":
		add_artists(db, args[1:])
	elif args[0] == "update":
		update_albums(db)
	elif args[0] == "show":
		show_new_albums(db, args[1])

def add_artists(db, args):
	cursor = db.cursor()

	names = []

	for name in set(args):
		cursor.execute("SELECT 1 FROM artist WHERE lower(name) = ?", [name.lower()])
		if cursor.fetchone():
			print "Artist already exists: %s" % name
		else:
			names.append(name)

	for name in names:
		print "Searching for artist: %s" % name
		match = search_artist(name)
		if match:
			id, name = match
			cursor.execute("SELECT name FROM artist WHERE artist_id = ?", [id])
			row = cursor.fetchone()
			if row:
				print "Updating artist: %s -> %s" % (row[0], name)
				cursor.execute("""UPDATE artist
				                  SET name = ?, stamp_updated = current_timestamp
				                  WHERE artist_id = ?""", [name, id])
			else:
				print "Adding artist: %s" % name
				cursor.execute("""INSERT INTO artist (artist_id, name, stamp_created)
				                  VALUES (?, ?, current_timestamp)""", [id, name])

	db.commit()

def update_albums(db):
	cursor = db.cursor()

	cursor.execute("SELECT current_timestamp")
	before, = cursor.fetchone()

	artists = []

	cursor.execute("SELECT artist_id, name FROM artist")
	for row in cursor:
		artists.append(row)

	for artist_id, artist_name in artists:
		print "Looking up artist: %s" % artist_name
		for album_id, album_name in lookup_artist_albums(artist_id):
			add_album = False
			add_album_artist = False

			cursor.execute("SELECT 1 FROM album WHERE album_id = ?", [album_id])
			if not cursor.fetchone():
				print "Adding album: %s" % album_name
				add_album = True
				add_album_artist = True
			else:
				cursor.execute("SELECT 1 FROM album_artist WHERE album_id = ? AND artist_id = ?", [album_id, artist_id])
				if not cursor.fetchone():
					print "Adding new artist for existing album: %s" % album_name
					add_album_artist = True

			if add_album:
				cursor.execute("""INSERT INTO album (album_id, name, stamp_created)
				                  VALUES (?, ?, current_timestamp)""", [album_id, album_name])

			if add_album_artist:
				cursor.execute("""INSERT INTO album_artist (album_id, artist_id, stamp_created)
				                  VALUES (?, ?, current_timestamp)""", [album_id, artist_id])

	db.commit()

def show_new_albums(db, since):
	cursor = db.cursor()

	cursor.execute("""SELECT album.stamp_created, album.album_id, artist.name, album.name
	                  FROM artist, album, album_artist
	                  WHERE album_artist.artist_id = artist.artist_id AND album_artist.album_id = album.album_id AND album.stamp_created >= ?
	                  ORDER BY artist.name, album.stamp_created DESC""", [since])

	for row in cursor:
		print "%s - %s - %s - %s" % row

def search_artist(search_name):
	uri = "http://ws.spotify.com/search/1/artist?" + urllib.urlencode(dict(q=search_name))

	page = urllib2.urlopen(uri)
	dom = domlib.parse(page)
	page.close()

	for element in dom.getElementsByTagName("artist"):
		artist_name = element.getElementsByTagName("name")[0].childNodes[0].data
		if artist_name.lower() == search_name.lower():
			artist_id = element.getAttribute("href")
			return artist_id, artist_name

	return None

def lookup_artist_albums(artist_id):
	uri = "http://ws.spotify.com/lookup/1/?" + urllib.urlencode(dict(uri=artist_id, extras="album"))

	page = urllib2.urlopen(uri)
	dom = domlib.parse(page)
	page.close()

	for element in dom.getElementsByTagName("album"):
		album_id = element.getAttribute("href")
		album_name = element.getElementsByTagName("name")[0].childNodes[0].data
		yield album_id, album_name

if __name__ == "__main__":
	main(sys.argv[1:])
