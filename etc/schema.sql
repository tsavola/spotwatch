CREATE TABLE artist (
	artist_id TEXT,
	name TEXT,
	stamp_created TIMESTAMP,
	stamp_updated TIMESTAMP
);

CREATE UNIQUE INDEX artist_id ON artist (artist_id);

CREATE TABLE album (
	album_id TEXT,
	name TEXT,
	stamp_created TIMESTAMP
);

CREATE UNIQUE INDEX album_id ON album (album_id);

CREATE TABLE album_artist (
	album_id TEXT,
	artist_id TEXT,
	stamp_created TIMESTAMP
);
